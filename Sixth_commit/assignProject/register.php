<!DOCTYPE html>
<html>
    <head>
     <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel = "stylesheet">
    </head>
    <body style="background-color:brown">
        
        <div class="row" style="background-color:red">
            <div class="col-3 mx-auto">
                <div class="text-center">
                    <h1>Library</h1>
                </div>
                    </div>
                        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">Contents</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="book.php">Book</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="aboutus.php">About Us</a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="Login.php">Log In</a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="register.php">Register</a>
              </li>
            </ul>
            </div>
        </nav>
        <div class="row" style="background-color:brown">
            <div class="col-4 mx-auto">
                <div class="text-center">
                   
               
        <h2>Registration form</h2>       
 
        <form method="post" action="">
          <div class="form-group">
            <label for="Username">Username</label>
            <input id="user" type="text" class="form-control" placeholder="Username" name="username">
          </div>
          <div class="form-group">
            <label for="Password">Password</label>
            <input id="pass" type="password" class="form-control"  placeholder="Password" name="password">
          </div>
          <div class="form-group">
            <label for="Confirm Password">Confirm Password</label>
            <input id="confpass" type="password" class="form-control" placeholder="Confirm Password" name="pword">
          </div>
            <div class="form-group">
            <label for="Name">Name</label>
            <input  id="name" type="text" class="form-control" placeholder="Name" name="name">
          </div>
            <div class="form-group">
            <label for="Name">Surname</label>
            <input id="surname" type="text" class="form-control" placeholder="Surname" name="surname">
          </div>
            <div class="form-group">
            <label for="Name">Email</label>
            <input id="email" type="email" class="form-control" placeholder="Email" name="email">
          </div>
          <button id="submit1" type="submit" name="submit" class="btn btn-primary">Register</button>
        </form>
            </div>
                </div>
                    </div>
        <?php
        if((empty($_POST['username'])) || (empty($_POST['password'])) || (empty($_POST['pword'])) || (empty($_POST['name'])) || (empty($_POST['surname']))||(empty($_POST['email'])) ){
            echo "
         <div class='row' style='background-color:brown'>
            <div class='col-2  mx-auto'>
                <div class='text-center'>
            All values must be set
            </div>
            </div>
             </div>
            "
            ;
        }else{
            if(!preg_match('/[^A-Za-z ]/', $_POST['name'])){
                //name only contains alphabet
                if(!preg_match('/[^A-Za-z ]/', $_POST['surname'])){
                    //surname only contains alphabet
                    //all statments passed
                     $conn = mysqli_connect("localhost","root","","library_db",3307);//which port to 3307 when in school,3306 when at home
                    if(mysqli_connect_errno()){
                            echo "Error: Could not connect to database. Please try again later";
                            exit;
                        }

                    $username = mysqli_real_escape_string($conn,$_POST['username']);
                    $password = mysqli_real_escape_string($conn,$_POST['password']);
                    $pword = mysqli_real_escape_string($conn,$_POST['pword']);
                    $name = mysqli_real_escape_string($conn,$_POST['name']);
                    $surname = mysqli_real_escape_string($conn,$_POST['surname']);
                    $email = mysqli_real_escape_string($conn,$_POST['email']);

                    if($password != $pword){
                        echo "
                        <div class='row' style='background-color:brown'>
                            <div class='col-2  mx-auto'>
                                <div class='text-center'>
                                    Password confirmation does not match password.
                                </div>
                            </div>
                        </div>";
                    } else{
                        $query = "SELECT COUNT(*) FROM user WHERE Username = '$username'";
                        $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        $row = mysqli_fetch_row($result);
                        $count = $row[0];
                        if($count <= 0){
                            $hashed_password = password_hash($password,PASSWORD_DEFAULT);

                            $query = "INSERT INTO user (Username,Password,Name,Surname,Email)
                            VALUES ('$username', '$hashed_password','$name','$surname','$email')";
                            mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        } else {
                            echo "
                        <div class='row' style='background-color:brown'>
                            <div class='col-2  mx-auto'>
                                <div class='text-center'>
                                    already exists
                                    </div>
                                </div>
                            </div>";
                        }

                        mysqli_free_result($result);
                        mysqli_close($conn);
                    }
                    
                }else{
                    echo"
                    <div class='row' style='background-color:brown'>
                            <div class='col-2  mx-auto'>
                                <div class='text-center'>
                    Suname must contain only letters
                        </div>
                    </div>
                </div>"
                          ;
                }
                
            }else{
                echo"
                <div class='row' style='background-color:brown'>
                            <div class='col-2  mx-auto'>
                                <div class='text-center'>
                                    Name must contain only letters
                </div>
                    </div>
                        </div>";
            }
            
  
        }
        ?>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>