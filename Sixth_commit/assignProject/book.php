<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel = "stylesheet">
    </head>
    <body style="background-color:brown">
        <div class="row" style="background-color:red">
            <div class="col-3 mx-auto">
                <div class="text-center">
                    <h1>Library</h1>
                </div>
                    </div>
                        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">Contents</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="book.php">Book</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="aboutus.php">About Us</a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="Login.php">Log In</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="register.php">Register</a>
              </li>
            </ul>

          </div>
        </nav>
          <div class="row" style="background-color:brown">
            <div class="col-4 mx-auto">
                <div class="text-center">
                    <h2>Book List</h2>
              
        <form  method="post" action="">
         <label for="search">Search</label> <select class="form-control" id="booklist" name="bookvalue">
                <option value="0">--Select Option--</option>
                <option value="1">ISBN</option>
                <option value="2">Book Name</option>
                <option value="3">Author</option>
                </select><input type="text"  class="form-control"name="search" placeholder="Search" >
            <button type="submit" name="btnsearch" class="btn btn-primary">Search</button>
          
        </form>
      
          </div>
                </div>
                    </div>
        
        
        
        
    <?php


    // Create connection
    $conn = mysqli_connect("localhost","root","","library_db",3307);//which port to 3307 when in school ,3306 when at home
            if(mysqli_connect_errno()){
                 echo "Error: Could not connect to database. Please try again later";
                    exit;
                }

    $query = "SELECT * FROM book";
      $result = mysqli_query($conn,$query)
            or die("Error in query: ".mysqli_error($conn));

    if ($result -> num_rows > 0) {
        // output data of each row
        echo"
        <div class='row' style='background-color:brown'>
            <div class='col-2  mx-auto'>
                <div class='text-center'>
                <table>

            <th>ISBN</th> 
            <th>BookName</th>
            <th>Author</th>
            <th>Genre</th>
            ";
        while($row = $result->fetch_assoc()) {
            echo "<tr> <td>" . $row["BookISBN"]. "</td><td> " . $row["BookName"]. "</td><td> " . $row["Author"]. "</td><td> " . $row["Genre"]."</td></tr>";
        }
    } else {
        echo "0 results";
    }
     echo"<table>
        </div>
        </div>
        </div>";
    //$conn->close();
        
        
        if(isset($_POST['btnsearch'])){
            echo"<br><br><br>";
            echo"<h3>Filtered list</h3>";
            $val = $_POST['bookvalue'];
            $search = $_POST['search'];
          
                if($val == "0"){
                    echo"Please Enter an Option";

                }else if($val == "1"){
                    if(strlen($search) == 13){
                        $query = "SELECT * FROM book WHERE BookISBN = $search";
                        $result = mysqli_query($conn,$query)
                            or die("Error in query: ".mysqli_error($conn));

                    if ($result -> num_rows > 0) {
                        // output data of each row
                        echo" <div class='row' style='background-color:brown'>
                            <div class='col-2 mx-auto'>
                            <div class='text-center'>
                        <table>

                            <th>ISBN</th> 
                            <th>BookName</th>
                            <th>Author</th>
                            <th>Genre</th>
                            ";
                        while($row = $result->fetch_assoc()) {
                            echo "<tr> <td>" . $row["BookISBN"]. "</td><td> " . $row["BookName"]. "</td><td> " . $row["Author"]. "</td><td> " . $row["Genre"]."</td></tr>";
                        }
                    } else {
                        echo "No results where found.";
                    }
                     echo"<table>
                        </div>
                        </div>
                        </div>";
                    }else{
                        echo"ISBN must be of length of 13";
                    }
                }else if($val == "2"){
                    if(!preg_match('/[^A-Za-z ]/', $search)){
                        $query = "SELECT * FROM book WHERE BookName = '$search'";
                        $result = mysqli_query($conn,$query)
                            or die("Error in query: ".mysqli_error($conn));

                    if ($result -> num_rows > 0) {
                        // output data of each row
                        echo" <div class='row' style='background-color:brown'>
                            <div class='col-2 mx-auto'>
                            <div class='text-center'>
                        <table>

                            <th>ISBN</th> 
                            <th>BookName</th>
                            <th>Author</th>
                            <th>Genre</th>
                            ";
                        while($row = $result->fetch_assoc()) {
                            echo "<tr> <td>" . $row["BookISBN"]. "</td><td> " . $row["BookName"]. "</td><td> " . $row["Author"]. "</td><td> " . $row["Genre"]."</td></tr>";
                        }
                    } else {
                        echo "No results where found.";
                    }
                     echo"<table>
                        </div>
                        </div>
                        </div>";
                    }else{
                        echo"BookName must contain only letters";
                    }
                    
                }else if ($val == "3"){
                   
                    if(!preg_match('/[^A-Za-z ]/', $search)){
                        $query = "SELECT * FROM book WHERE Author = '$search'";
                        $result = mysqli_query($conn,$query)
                            or die("Error in query: ".mysqli_error($conn));

                    if ($result -> num_rows > 0) {
                        // output data of each row
                        echo"<div class='row' style='background-color:brown'>
                            <div class='col-2 mx-auto'>
                            <div class='text-center'>
                            <table>

                            <th>ISBN</th> 
                            <th>BookName</th>
                            <th>Author</th>
                            <th>Genre</th>
                            ";
                        while($row = $result->fetch_assoc()) {
                            echo "<tr> <td>" . $row["BookISBN"]. "</td><td> " . $row["BookName"]. "</td><td> " . $row["Author"]. "</td><td> " . $row["Genre"]."</td></tr>";
                        }
                    } else {
                        echo "No results where found.";
                    }
                     echo"<table>
                        </div>
                        </div>
                        </div>";
                    }else{
                        echo"Author must only contain letters";
                    }
                }
        
        
        
        
        
        
    }
        
    ?>

        
        
    <script src="js/bootstrap.min.js"></script>
        <div class="row">
            <div class="col-12 mx-auto">
                <div class="text-center">
      <a class="btn btn-secondary" href="addbook.php" role="button">Add Book</a>
        <a class="btn btn-secondary" href="removebook.php" role="button">Remove Book</a>
                </div>
            </div>
        </div>
    </body>
</html>