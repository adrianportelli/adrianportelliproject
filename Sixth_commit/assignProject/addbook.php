<html> 
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel = "stylesheet">
    </head>
    <body style="background-color:brown">   
            <div class="row" style="background-color:red">
            <div class="col-3 mx-auto">
                <div class="text-center">
                    <h1>Library</h1>
                </div>
                    </div>
                        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">Contents</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="book.php">Book</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="aboutus.php">About Us</a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="Login.php">Log In</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="register.php">Register</a>
              </li>
            </ul>

          </div>
        </nav>
        
        <div class="row" style="background-color:brown">
            <div class="col-4 mx-auto">
                <div class="text-center">
                    <h2>Add Book</h2>

        <form method="post" action="">
          <div class="form-group">
            <label for="Book">Book 13-ISBN: </label>
            <input type="number" class="form-control" placeholder="Book ISBN" name="isbn">
          </div>
          <div class="form-group">
            <label for="BookName">BookName</label>
            <input type="text" class="form-control"  placeholder="BookName" name="name">
          </div>
          <div class="form-group">
            <label for="Author">Author</label>
            <input type="text" class="form-control" placeholder="Author" name="author">
          </div>
            <div class="form-group">
            <label for="Name">Genre</label>
            <input type="text" class="form-control" placeholder="Genre" name="genre">
          </div>
          <button type="submit" name="submit" class="btn btn-primary">Add</button>
        </form>
                </div>
            </div>
        </div>

<?php

     if((empty($_POST['isbn'])) || (empty($_POST['name'])) || (empty($_POST['author'])) || (empty($_POST['genre']))){
            echo "
            <div class='row' style='background-color:brown'>
                <div class='col-2  mx-auto'>
                    <div class='text-center'>
                    All values must be set
                    </div>
                </div>
            </div>";
        }else{
            if(strlen($_POST['isbn']) == 13){
                //isbn has length of 16
                if (is_numeric($_POST['isbn'])) {
                    //isbn is numberic
                        if(!preg_match('/[^A-Za-z ]/', $_POST['name'])){
                        //name only contains alphabet
                            if(!preg_match('/[^A-Za-z ]/', $_POST['author'])){
                                //author only contains alphabet
                                if(!preg_match('/[^A-Za-z ]/', $_POST['genre'])){
                                    //genre only contains alphabet
                                    //all statments passed
                                    $conn = mysqli_connect("localhost","root","","library_db",3307);//which port to 3307 when in school,3306 when at home
                                    if(mysqli_connect_errno()){
                                            echo "Error: Could not connect to database. Please try again later";
                                            exit;}
            
                                    $isbn = mysqli_real_escape_string($conn,$_POST['isbn']);
                                    $bookname = mysqli_real_escape_string($conn,$_POST['name']);
                                    $author = mysqli_real_escape_string($conn,$_POST['author']);
                                    $genre = mysqli_real_escape_string($conn,$_POST['genre']);
           
                                    $query = "SELECT COUNT(*) FROM book WHERE BookISBN = '$isbn'";
                                    $result = mysqli_query($conn, $query)
                                        or die("Error in query: 1". mysqli_error($conn));
                                    
                                    $row = mysqli_fetch_row($result);
                                    $count = $row[0];
                                    
                                    if($count <= 0){
                                        
                                        $query = "INSERT INTO book (BookISBN,BookName,Author,Genre)
                                        VALUES ('$isbn', '$bookname','$author','$genre')";
                                        mysqli_query($conn, $query)
                                        or die("Error in query: 2". mysqli_error($conn));
                                    } else {
                                        echo "
                                                    <div class='row' style='background-color:brown'>
                                                        <div class='col-2  mx-auto'>
                                                            <div class='text-center'>
                                                                Book already is created
                                                                </div>
                                                            </div>
                                                        </div>";
                                    }

                                    mysqli_free_result($result);
                                    mysqli_close($conn);

                                }else{
                                    echo"
                                        <div class='row' style='background-color:brown'>
                                            <div class='col-2  mx-auto'>
                                                <div class='text-center'>
                                                  Genre must only contain letters
                                                  </div>
                                                </div>
                                            </div>";
                                }
                                
                            }else{
                                //author does not only contains alphabet
                                echo"
                                <div class='row' style='background-color:brown'>
                                    <div class='col-2  mx-auto'>
                                        <div class='text-center'>
                                            Author must only contain letters
                                        </div>
                                    </div>
                                </div>"
                                            ;
                            }
                        }else{
                            //name does not only contains alphabet
	                       echo"
                            <div class='row' style='background-color:brown'>
                                    <div class='col-2  mx-auto'>
                                        <div class='text-center'>
                                            BookName must contain only letters
                                        </div>
                                    </div>
                                </div>";
                        }
                         
                } else {
                        //isbn not numberic
                        echo"
                         <div class='row' style='background-color:brown'>
                                    <div class='col-2  mx-auto'>
                                        <div class='text-center'>
                                            ISBN must only contain numbers
                                        </div>
                                    </div>
                            </div>";
                    }
            }else{
                //ISBN not of lenght 16
                echo"
                <div class='row' style='background-color:brown'>
                                    <div class='col-2  mx-auto'>
                                        <div class='text-center'>
                                            ISBN must be of length 16
                                            </div>
                                        </div>
                                    </div>";
            }
           
            }
        





?>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>