<?php


// Create connection
$conn = mysqli_connect("localhost","root","","library_db",3306);//which port to 3307 when in school ,3306 when at home
        if(mysqli_connect_errno()){
             echo "Error: Could not connect to database. Please try again later";
                exit;
            }

$query = "SELECT * FROM book";
  $result = mysqli_query($conn,$query)
        or die("Error in query: ".mysqli_error($conn));

if ($result -> num_rows > 0) {
    // output data of each row
    echo"<table>
    
        <th>ISBN</th> 
        <th>BookName</th>
        <th>Author</th>
        <th>Genre</th>
        ";
    while($row = $result->fetch_assoc()) {
        echo "<tr> <td>" . $row["BookISBN"]. "</td><td> " . $row["BookName"]. "</td><td> " . $row["Author"]. "</td><td> " . $row["Genre"]."</td></tr>";
    }
} else {
    echo "0 results";
}
 echo"<table>";
$conn->close();
?>
