<html> 
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel = "stylesheet">
    </head>
<body>   
             
        <form method="post" action="">
          <div class="form-group">
            <label for="Book">Book 13-ISBN: </label>
            <input type="number" class="form-control" placeholder="Book ISBN" name="isbn">
          </div>
          <div class="form-group">
            <label for="BookName">BookName</label>
            <input type="text" class="form-control"  placeholder="BookName" name="name">
          </div>
          <div class="form-group">
            <label for="Author">Author</label>
            <input type="text" class="form-control" placeholder="Author" name="author">
          </div>
            <div class="form-group">
            <label for="Name">Genre</label>
            <input type="text" class="form-control" placeholder="Genre" name="genre">
          </div>
          <button type="submit" name="submit" class="btn btn-primary">Add</button>
        </form>

<?php

     if((empty($_POST['isbn'])) || (empty($_POST['name'])) || (empty($_POST['author'])) || (empty($_POST['genre']))){
            echo "All values must be set";
        }else{
            if(strlen($_POST['isbn']) == 13){
                //isbn has length of 16
                if (is_numeric($_POST['isbn'])) {
                    //isbn is numberic
                        if(!preg_match('/[^A-Za-z ]/', $_POST['name'])){
                        //name only contains alphabet
                            if(!preg_match('/[^A-Za-z ]/', $_POST['author'])){
                                //author only contains alphabet
                                if(!preg_match('/[^A-Za-z ]/', $_POST['genre'])){
                                    //genre only contains alphabet
                                    //all statments passed
                                    $conn = mysqli_connect("localhost","root","","library_db",3306);//which port to 3307 when in school,3306 when at home
                                    if(mysqli_connect_errno()){
                                            echo "Error: Could not connect to database. Please try again later";
                                            exit;}
            
                                    $isbn = mysqli_real_escape_string($conn,$_POST['isbn']);
                                    $bookname = mysqli_real_escape_string($conn,$_POST['name']);
                                    $author = mysqli_real_escape_string($conn,$_POST['author']);
                                    $genre = mysqli_real_escape_string($conn,$_POST['genre']);
           
                                    $query = "SELECT COUNT(*) FROM book WHERE BookISBN = '$isbn'";
                                    $result = mysqli_query($conn, $query)
                                        or die("Error in query: ". mysqli_error($conn));
                                    
                                    $row = mysqli_fetch_row($result);
                                    $count = $row[0];
                                    
                                    if($count <= 0){
                                        
                                        $query = "INSERT INTO book (BookISBN,BookName,Author,Genre)
                                        VALUES ('$isbn', '$bookname','$author','$genre')";
                                        mysqli_query($conn, $query)
                                        or die("Error in query: ". mysqli_error($conn));
                                    } else {
                                        echo "Book already is created";
                                    }

                                    mysqli_free_result($result);
                                    mysqli_close($conn);

                                }else{
                                    echo"Genre must only contain letters";
                                }
                                
                            }else{
                                //author does not only contains alphabet
                                echo"Author must only contain letters";
                            }
                        }else{
                            //name does not only contains alphabet
	                       echo"BookName must contain only letters";
                        }
                         
                } else {
                        //isbn not numberic
                        echo"ISBN must only contain numbers";
                    }
            }else{
                //ISBN not of lenght 16
                echo"ISBN must be of length 16";
            }
           
            }
        





?>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>