<!DOCTYPE html>
<html>
    <head>
     <meta charset="utf-8">
    <link href="css/bootstrap.min.css" rel = "stylesheet">
    </head>
    <body>
        <h1>Registration form</h1>       
 
        <form method="post" action="">
          <div class="form-group">
            <label for="Username">Username</label>
            <input type="text" class="form-control" placeholder="Username" name="username">
          </div>
          <div class="form-group">
            <label for="Password">Password</label>
            <input type="password" class="form-control"  placeholder="Password" name="password">
          </div>
          <div class="form-group">
            <label for="Confirm Password">Confirm Password</label>
            <input type="password" class="form-control" placeholder="Password" name="pword">
          </div>
            <div class="form-group">
            <label for="Name">Name</label>
            <input type="text" class="form-control" placeholder="Name" name="name">
          </div>
            <div class="form-group">
            <label for="Name">Surname</label>
            <input type="text" class="form-control" placeholder="Surname" name="surname">
          </div>
            <div class="form-group">
            <label for="Name">Email</label>
            <input type="email" class="form-control" placeholder="Email" name="email">
          </div>
          <button type="submit" name="submit" class="btn btn-primary">Register</button>
        </form>
        <?php
        if((empty($_POST['username'])) || (empty($_POST['password'])) || (empty($_POST['pword'])) || (empty($_POST['name'])) || (empty($_POST['surname']))||(empty($_POST['email'])) ){
            echo "All values must be set";
        }else{
            if(!preg_match('/[^A-Za-z ]/', $_POST['name'])){
                //name only contains alphabet
                if(!preg_match('/[^A-Za-z ]/', $_POST['surname'])){
                    //surname only contains alphabet
                    //all statments passed
                     $conn = mysqli_connect("localhost","root","","library_db",3306);//which port to 3307 when in school,3306 when at home
                    if(mysqli_connect_errno()){
                            echo "Error: Could not connect to database. Please try again later";
                            exit;
                        }

                    $username = mysqli_real_escape_string($conn,$_POST['username']);
                    $password = mysqli_real_escape_string($conn,$_POST['password']);
                    $pword = mysqli_real_escape_string($conn,$_POST['pword']);
                    $name = mysqli_real_escape_string($conn,$_POST['name']);
                    $surname = mysqli_real_escape_string($conn,$_POST['surname']);
                    $email = mysqli_real_escape_string($conn,$_POST['email']);

                    if($password != $pword){
                        echo "Password confirmation does not match password.";
                    } else{
                        $query = "SELECT COUNT(*) FROM user WHERE Username = '$username'";
                        $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        $row = mysqli_fetch_row($result);
                        $count = $row[0];
                        if($count <= 0){
                            $hashed_password = password_hash($password,PASSWORD_DEFAULT);

                            $query = "INSERT INTO user (Username,Password,Name,Surname,Email)
                            VALUES ('$username', '$hashed_password','$name','$surname','$email')";
                            mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        } else {
                            echo "already exists";
                        }

                        mysqli_free_result($result);
                        mysqli_close($conn);
                    }
                    
                }else{
                    echo"Suname must contain only letters";
                }
                
            }else{
                echo"Name must contain only letters";
            }
            
  
        }
        ?>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>