<html> 
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel = "stylesheet">
    </head>
    <body  style="background-color:brown">
        <div class="row" style="background-color:red">
            <div class="col-3 mx-auto">
                <div class="text-center">
                    <h1>Library</h1>
                </div>
                    </div>
                        </div>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
          <a class="navbar-brand" href="#">Contents</a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item active">
                <a class="nav-link" href="book.php">Book</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">About Us</a>
              </li>
             <li class="nav-item">
                <a class="nav-link" href="Login.php">Log In</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="register.php">Register</a>
              </li>
            </ul>

          </div>
        </nav>
        <div class="row" style="background-color:brown">
            <div class="col-4 mx-auto">
                <div class="text-center">
                    <h2>Remove Book</h2>
                 
        <form method="post" action="">
          <div class="form-group">
            <label for="Bookisbn">BookISBN</label>
            <input type="number" class="form-control" placeholder="BookISBN" name="isbn">
          </div>
          <div class="form-group">
            <label for="BookName">BookName</label>
            <input type="text" class="form-control"  placeholder="BookName" name="name">
          <button type="submit" name="submit" class="btn btn-primary">Remove</button>
            </div>
        </form>
                </div>
            </div>
        </div>
        <?php

             if((empty($_POST['isbn'])) || (empty($_POST['name']))){
                    echo "
                    <div class='row' style='background-color:brown'>
                        <div class='col-2  mx-auto'>
                            <div class='text-center'>
                                All values must be set
                            </div>
                        </div>
                    </div>";
                }else{
                    $conn = mysqli_connect("localhost","root","","library_db",3307);//which port to 3307 when in school,3306 when at home
                    if(mysqli_connect_errno()){
                            echo "Error: Could not connect to database. Please try again later";
                            exit;
                        }

                    $isbn = mysqli_real_escape_string($conn,$_POST['isbn']);
                    $bookname = mysqli_real_escape_string($conn,$_POST['name']);

                        $query = "SELECT COUNT(*) FROM book WHERE BookISBN = '$isbn' and BookName = '$bookname'";
                        $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        $row = mysqli_fetch_row($result);

                        $count = $row[0];
                        if($count > 0){

                            $query2 = "DELETE FROM book WHERE BookISBN = '$isbn'";
                            echo"
                            <div class='row' style='background-color:brown'>
                                <div class='col-2  mx-auto'>
                                    <div class='text-center'>
                                        Book Deleted
                                    </div>
                                </div>
                            </div>";
                            mysqli_query($conn, $query2)
                            or die("Error in query: ". mysqli_error($conn));

                        } 

                        mysqli_free_result($result);
                        mysqli_close($conn);
                    }






        ?>
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>