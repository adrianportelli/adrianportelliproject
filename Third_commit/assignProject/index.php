<!DOCTYPE html>
<html>
    <head>
    
    </head>
    <body>
        <h1>Registration form</h1>       
        <form method="post" action="">
            <p>Username: <input type="text" name="username"></p>
            <p>Password: <input type="password" name="password"></p>
            <p>Confirm password: <input type="password" name="pword"></p>
            <p>Name: <input type="text" name="name"></p>
            <p>Surname: <input type="text" name="surname"></p>
            <p>Email: <input type="email" name="email"></p>
            <p><input type="submit" name="submit" value="Log In"></p>   
        </form>
        <?php
        if((empty($_POST['username'])) || (empty($_POST['password'])) || (empty($_POST['pword'])) || (empty($_POST['name'])) || (empty($_POST['surname']))||(empty($_POST['email'])) ){
            echo "All values must be set";
        }else{
            if(!preg_match('/[^A-Za-z ]/', $_POST['name'])){
                //name only contains alphabet
                if(!preg_match('/[^A-Za-z ]/', $_POST['surname'])){
                    //surname only contains alphabet
                    //all statments passed
                     $conn = mysqli_connect("localhost","root","","library_db",3306);//which port to 3307 when in school,3306 when at home
                    if(mysqli_connect_errno()){
                            echo "Error: Could not connect to database. Please try again later";
                            exit;
                        }

                    $username = mysqli_real_escape_string($conn,$_POST['username']);
                    $password = mysqli_real_escape_string($conn,$_POST['password']);
                    $pword = mysqli_real_escape_string($conn,$_POST['pword']);
                    $name = mysqli_real_escape_string($conn,$_POST['name']);
                    $surname = mysqli_real_escape_string($conn,$_POST['surname']);
                    $email = mysqli_real_escape_string($conn,$_POST['email']);

                    if($password != $pword){
                        echo "Password confirmation does not match password.";
                    } else{
                        $query = "SELECT COUNT(*) FROM user WHERE Username = '$username'";
                        $result = mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        $row = mysqli_fetch_row($result);
                        $count = $row[0];
                        if($count <= 0){
                            $hashed_password = password_hash($password,PASSWORD_DEFAULT);

                            $query = "INSERT INTO user (Username,Password,Name,Surname,Email)
                            VALUES ('$username', '$hashed_password','$name','$surname','$email')";
                            mysqli_query($conn, $query)
                            or die("Error in query: ". mysqli_error($conn));
                        } else {
                            echo "already exists";
                        }

                        mysqli_free_result($result);
                        mysqli_close($conn);
                    }
                    
                }else{
                    echo"Suname must contain only letters";
                }
                
            }else{
                echo"Name must contain only letters";
            }
            
  
        }
        ?>
    </body>
</html>